﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;

namespace Bitkep
{
    class HeightMap
    {
        const int SIZEX = 1201;
        const int SIZEY = 1201;
        int[,] elevation = new int [SIZEX, SIZEY];

        public int ElevationThreshold { get; set; }

        public static HeightMap Parse(string path)
        {
            HeightMap map = new HeightMap();
            FileStream file = new FileStream(path, FileMode.Open);
            using (BinaryReader br = new BinaryReader(file))
            {
                byte[] memblock = new byte[2];
                for (int i = 0; i < SIZEX; i++)
                {
                    for (int j = 0; j < SIZEY; j++)
                    {
                        memblock = br.ReadBytes(2);
                            map.elevation[i, j] = (memblock[0] << 8 | memblock[1]);
                    }
                }
            }
            file.Close();
            return map;

        }

        private int[] GetExtremalElevations()
        {
            int[] minMax = new int[2];
            int min = sizeof(int);
            int max = 0;
            foreach (var item in elevation)
            {
                if (item > max)
                    max = item;
            }

            foreach (var item in elevation)
            {
                if (item < min)
                    min = item;
            }

            minMax[0] = min;
            minMax[1] = max;

            return minMax;
        }

        public void SaveToBitmap(string path)
        {
            Bitmap bmp = new Bitmap(SIZEX, SIZEY);

            for (int i = 0; i < SIZEX; i++)
            {
                for (int j = 0; j < SIZEY; j++)
                {
                    int value=0;
                    if (this.elevation[i, j] < 256)
                        value = this.elevation[i, j];
                    else
                        value = 255;
                    Color c = Color.FromArgb(0, value, 0);
                    Color b = Color.Aqua;
                    bmp.SetPixel(i, j, c);
                    if (this.elevation[i, j] <= ElevationThreshold)
                        bmp.SetPixel(i, j, b);
                }
            }
            bmp.Save(path + ".bmp");
        }
    }
}
