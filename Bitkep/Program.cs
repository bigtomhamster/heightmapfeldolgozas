﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;

namespace Bitkep
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length > 2)
            {
                string input = "";
                string output = "";
                input += args[0];
                output += args[1];
                HeightMap map = HeightMap.Parse(input);
                map.ElevationThreshold = int.Parse(args[2]);
                map.SaveToBitmap(output);
                //Console.ReadKey();
            }
            else
            {
                Console.WriteLine("Adj paramétereket pls");
                Console.ReadKey();

            }
        }
    }
}
